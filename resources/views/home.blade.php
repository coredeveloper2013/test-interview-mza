@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                  <?php echo Auth::user()->user_type ; ?>  You are logged in!
                </div>
                <?php if((Auth::user()->user_type)== "sysAdmin"){?>
                <div class="card">

                    <div class="card-header">Create Admin</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('createadmin') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="user_type" class="col-md-4 col-form-label text-md-right">User Type</label>

                                <div class="col-md-6">
                                    <input id="user_type" type="text" class="form-control" name="user_type" value="Admin" disabled required>
                                </div>
                            </div>
                           <!-- <input name="user_type" type="hidden" value="sysAdmin"> -->
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create admin
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
              <?php }else if ((Auth::user()->user_type)== "Admin"){ ?>
                <div class="card">

                    <div class="card-header">Create Manager</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('createmanager') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="user_type" class="col-md-4 col-form-label text-md-right">User Type</label>

                                <div class="col-md-6">
                                    <input id="user_type" type="text" class="form-control" name="user_type" value="Admin" disabled required>
                                </div>
                            </div>
                           <!-- <input name="user_type" type="hidden" value="sysAdmin"> -->
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create Manager
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
              <?php } ?>
            </div>
        </div>
      <?php if ((Auth::user()->user_type)== "Manager"){ ?>

        <div class="card">

            <div class="card-header">Customers List</div>

            <div class="card-body">

       <div class="col-md-12">
     <table class="table table-responsive  table-striped table-bordered table-hover" style="width:100%;">
         <thead style="background-color:#d9edf7;">

            <th>Nmae</th>
            <th>Email</th>

         </thead>
         <tbody>
           @foreach($customer as $customer)
               <tr >

                   <td>{{$customer->name}}</td>
                  <td>{{$customer->email}}</td>
                </tr>
           @endforeach

         </tbody>

     </table>

</div>
</div>
</div>
<?php } ?>
    </div>
</div>
@endsection
