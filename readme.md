#Get the source code on your machine via git
 - clone project
 - Rename file .env.example to .env and change credentials.
 - Create a database and inform .env

#go to project folder
- cd projectname
- composer update
- php artisan key:generate
- php artisan migrate --seed to create and      populate tables
- php -S localhost:8000 -t public to start the app on http://localhost:8000/

system admin user name:admin@test.com password:123456
